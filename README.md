# Hexad recruitment test

## Question

The test containing a challenging and interesting question can be found in 
`question.pdf`.

## Solution 

This is the Java solution. Some assumptions made:

- The order must be fulfilled exactly.
  An order of 9 VS5 must be met as 3 x 3 rather than 2 x 5, even though the 
  latter requires less packs.
- Any order that cannot be exactly fulfilled must be raised till it can.
  An order of 7 VS5 must be raised to 8 and then fulfilled as 1 x 5 + 1 x 3.   

## How-to ...?

### Test

To run the tests with Gradle, use the following command:
```
$ ./gradlew test
```

If all tests pass, the runner will exit with code 0.

### Run

To run the code with Gradle, use the following command:
```
$ ./gradlew run
```

When the Gradle daemon reaches the task `run`, enter the following input:
```
10 vs5
14 mb11
13 cf
```

Your expected output will be printed to the screen.

### JAR

You can also build a JAR of the application with Gradle, as follows:
```
$ ./gradlew clean jar
```

This will place a JAR file at `build/libs/hexad.jar`. 
To run the JAR file with the Kotlin stdlib, enter, invoke the following command:
```
$ java -cp kotlin-stdlib-1.3.61.jar:build/libs/hexad.jar MainKt 
```

At the prompt, enter the following input:
```
10 vs5
14 mb11
13 cf
```

Your expected output will be printed to the screen.
