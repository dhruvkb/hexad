import org.junit.jupiter.api.Assertions.*

internal class UtilitiesKtTest {
    @org.junit.jupiter.api.Test
    fun `contains three given items`() {
        val menu = getMenu()

        assertEquals(menu.keys.size, 3)
        assertEquals(menu.keys.contains("VS5"), true)
        assertEquals(menu.keys.contains("MB11"), true)
        assertEquals(menu.keys.contains("CF"), true)
    }

    @org.junit.jupiter.api.Test
    fun `correctly prints one digit`() {
        assertEquals(currencyPrint(1), "$0.01")
    }

    @org.junit.jupiter.api.Test
    fun `correctly prints two digits`() {
        assertEquals(currencyPrint(10), "$0.10")
    }

    @org.junit.jupiter.api.Test
    fun `correctly prints three digits`() {
        assertEquals(currencyPrint(100), "$1.00")
    }

    @org.junit.jupiter.api.Test
    fun `correctly prints four digits`() {
        assertEquals(currencyPrint(1000), "$10.00")
    }
}