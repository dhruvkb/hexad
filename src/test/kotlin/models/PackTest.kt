package models

import org.junit.jupiter.api.Assertions.assertEquals
import java.io.ByteArrayOutputStream
import java.io.PrintStream

internal class PackTest {
    private val outContent = ByteArrayOutputStream()
    private val errContent = ByteArrayOutputStream()

    private val originalOut = System.out
    private val originalErr = System.err

    @org.junit.jupiter.api.BeforeEach
    fun redirectStreams() {
        System.setOut(PrintStream(outContent))
        System.setErr(PrintStream(errContent))
    }

    @org.junit.jupiter.api.Test
    fun `prints pack in correct format`() {
        pack.print(3)
        val expectedOutput = "\t3 x 2 ($1.00) = $3.00\n"

        assertEquals(expectedOutput, outContent.toString())
    }

    @org.junit.jupiter.api.AfterEach
    fun restoreStreams() {
        System.setOut(originalOut)
        System.setErr(originalErr)
    }

    companion object {
        private lateinit var pack: Pack

        @org.junit.jupiter.api.BeforeAll
        @JvmStatic
        internal fun setup() {
            pack = Pack(2, 100)
        }
    }
}