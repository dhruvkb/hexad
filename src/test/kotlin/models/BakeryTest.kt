package models

import getMenu
import org.junit.jupiter.api.Assertions.assertEquals

internal class BakeryTest {

    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.Order(1)
    fun `adds order for new dish`() {
        bakery.addOrder("10 VS5")

        assertEquals(bakery.getOrder("VS5").units, 10)
    }

    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.Order(2)
    fun `updates order for existing dish`() {
        bakery.addOrder("5 VS5")

        assertEquals(bakery.getOrder("VS5").units, 15)
    }

    companion object {
        private lateinit var bakery: Bakery

        @org.junit.jupiter.api.BeforeAll
        @JvmStatic
        internal fun setup() {
            bakery = Bakery(getMenu())
        }
    }
}