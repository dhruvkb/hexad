package models

import getMenu
import org.junit.jupiter.api.Assertions.assertEquals

internal class DishTest {

    @org.junit.jupiter.api.Test
    fun `packs reachable units correctly`() {
        val packing = dish.getPacking(8)

        assertEquals(packing.getValue(dish.getPack(3)), 1)
        assertEquals(packing.getValue(dish.getPack(5)), 1)
    }

    @org.junit.jupiter.api.Test
    fun `prefers matching over packing`() {
        val packing = dish.getPacking(9)

        assertEquals(packing.getValue(dish.getPack(3)), 3)
        assertEquals(packing.getValue(dish.getPack(5)), 0)
    }

    @org.junit.jupiter.api.Test
    fun `increments units when unreachable`() {
        val packing = dish.getPacking(4)

        assertEquals(packing.getValue(dish.getPack(3)), 0)
        assertEquals(packing.getValue(dish.getPack(5)), 1)
    }

    companion object {
        private lateinit var dish: Dish

        @org.junit.jupiter.api.BeforeAll
        @JvmStatic
        internal fun setup() {
            dish = getMenu().getValue("VS5")
        }
    }
}