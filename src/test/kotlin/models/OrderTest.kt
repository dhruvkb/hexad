package models

import getMenu
import org.junit.jupiter.api.Assertions.assertEquals
import java.io.ByteArrayOutputStream
import java.io.PrintStream

internal class OrderTest {
    private val outContent = ByteArrayOutputStream()
    private val errContent = ByteArrayOutputStream()

    private val originalOut = System.out
    private val originalErr = System.err

    @org.junit.jupiter.api.BeforeEach
    fun redirectStreams() {
        System.setOut(PrintStream(outContent))
        System.setErr(PrintStream(errContent))
    }

    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.Order(1)
    fun `adds count to units`() {
        order.addUnits(4)

        assertEquals(order.units, 4)
    }

    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.Order(2)
    fun `calculates cost and units when finalised`() {
        order.finalise()

        assertEquals(order.totalUnits, 5)
        assertEquals(order.totalCost, 899)
    }

    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.Order(3)
    fun print() {
        order.print()
        val expectedOutput = "4 -> 5: VS5 (Vegemite Scoll) = $8.99\n"

        assertEquals(outContent.toString().startsWith(expectedOutput), true)
    }

    @org.junit.jupiter.api.AfterEach
    fun restoreStreams() {
        System.setOut(originalOut)
        System.setErr(originalErr)
    }

    companion object {
        private lateinit var order: Order

        @org.junit.jupiter.api.BeforeAll
        @JvmStatic
        internal fun setup() {
            order = Order(getMenu().getValue("VS5"), 0)
        }
    }
}