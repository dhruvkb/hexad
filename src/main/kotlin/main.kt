import java.util.Scanner

import models.Bakery

fun main() {
    val bakery = Bakery(getMenu())

    val reader = Scanner(System.`in`)
    while (true) {
        val orderLine: String = reader.nextLine()
        if (orderLine == "") break

        bakery.addOrder(orderLine)
    }
    bakery.print()
}