package models

/**
 * A bakery is a shop with a [menu] full of confections to choose from.
 */
class Bakery(private val menu: MutableMap<String, Dish>) {
    private val orders = mutableMapOf<Dish, Order>()

    /**
     * Create or update an order for a dish using [orderLine] read from STDIN.
     *
     * The order line must be of the format of an integer count followed by a
     * space followed by a string dish code.
     */
    fun addOrder(orderLine: String) {
        val (count, dishCode) = orderLine.split(" ")
        val order = getOrder(dishCode)

        order.addUnits(count.toInt())
    }

    /**
     * Get the order corresponding to the dish with the given [dishCode].
     */
    fun getOrder(dishCode: String): Order {
        val dish = menu.getValue(dishCode.toUpperCase())

        return if (orders.contains(dish)) {
            orders.getValue(dish)
        } else {
            val order =  Order(dish, 0)
            orders[dish] = order
            order
        }
    }

    /**
     * Print the invoice of all orders issued to the bakery.
     */
    fun print() {
        for ((_, order) in orders) {
            order.finalise()
            order.print()
        }
    }
}