package models

import currencyPrint

/**
 * A packs is a chunk of a collection of dishes with a specific [size]
 * and a definite [cost].
 */
class Pack(val size: Int, val cost: Int) {

    /**
     * Print the cost of a given [quantity] of packs.
     */
    fun print(quantity: Int) {
        val individualCost = currencyPrint(cost)
        val totalCost = currencyPrint(quantity * cost)

        println("\t$quantity x $size ($individualCost) = $totalCost")
    }
}