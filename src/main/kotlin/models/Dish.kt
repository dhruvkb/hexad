package models

/**
 * A dish is a confection prepared by the bakery with a definite [code],
 * a given [name] and a certain configuration of [packs].
 */
class Dish(
    val name: String,
    val code: String,
    private val packs: MutableMap<Int, Pack>
) {

    /**
     * Get the pack of the given [size] for this dish.
     */
    fun getPack(size: Int): Pack {
        return packs.getValue(size)
    }

    /**
     * Get a blank mapping of pack size with quantity.
     */
    private fun getBlank(): MutableMap<Int, Int> {
        val solution = mutableMapOf<Int, Int>()
        for ((size, _) in packs) {
            solution[size] = 0
        }

        return solution
    }

    /**
     * Check if the given [solution] of size-quantity mapping satisfies the [units]
     * in the order.
     */
    private fun isValid(solution: MutableMap<Int, Int>, units: Int): Boolean {
        var sum = 0
        for ((size, quantity) in solution) {
            sum += size * quantity
        }

        return sum >= units
    }

    /**
     * Update the solution [table] to reach the given [units].
     *
     * This algorithm implements dynamic programming to prevent unnecessary
     * recalculations, making it more efficient that the recursive algorithm.
     */
    private fun updateTable(
        units: Int,
        table: MutableList<Pair<Int, MutableMap<Int, Int>>>
    ) {
        for (i in table.size..units) {
            var res = if (i == 0) 0 else Int.MAX_VALUE
            val solution = getBlank()

            for (size in solution.keys.filter { it <= i }) {
                val subRes = table[i - size].first
                if (subRes != Int.MAX_VALUE && subRes + 1 < res) {
                    val subSolution = table[i - size].second

                    res = subRes + 1
                    solution[size] = solution[size]!! + 1
                    for ((subSize, quantity) in subSolution) {
                        solution[subSize] = solution[subSize]!! + quantity
                    }
                }
            }

            table.add(Pair(res, solution))
        }
    }

    /**
     * Get the packing configuration for a given number of [units] of the dish.
     *
     * The units will be increased till an exact solution is found.
     */
    fun getPacking(units: Int): Map<Pack, Int> {
        var updatedUnits = units
        var solution: MutableMap<Int, Int>
        val table = mutableListOf<Pair<Int, MutableMap<Int, Int>>>()

        do {
            updateTable(updatedUnits, table)
            solution = table[updatedUnits].second

            updatedUnits++  // Increase units...
        } while (!isValid(solution, units))  // ...till a solution is found

        return solution.mapKeys { packs.getValue(it.key) }
    }
}