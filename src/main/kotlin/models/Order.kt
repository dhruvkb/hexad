package models

import currencyPrint

/**
 * An order is a mapping of a [dish] with a number of [units] of that dish.
 */
class Order(private val dish: Dish, var units: Int) {
    private lateinit var packing: Map<Pack, Int>

    var totalCost: Int = 0
    /**
     * This is the number of units after packing. This can be greater than or
     * equal to units.
     */
    var totalUnits: Int = 0

    /**
     * Add more units of the given dish.
     */
    fun addUnits(count: Int) {
        units += count
    }

    /**
     * Finalise the order and determine packing configuration.
     */
    fun finalise() {
        packing = dish.getPacking(units)

        for ((pack, quantity) in packing) {
            totalCost += quantity * pack.cost
            totalUnits += quantity * pack.size
        }
    }

    /**
     * Print the order with quantity, cost and breakdown.
     */
    fun print() {
        if (totalUnits == units) {
            // Order could be exactly fulfilled
            print("$units")
        } else {
            // Order was raised till it could be exactly fulfilled
            print("$units -> $totalUnits")
        }
        println(": ${dish.code} (${dish.name}) = ${currencyPrint(totalCost)}")

        for ((pack, quantity) in packing.filter { it.value != 0 }) {
            pack.print(quantity)
        }
    }
}