import models.Dish
import models.Pack

fun getMenu(): MutableMap<String, Dish> {
    val vs5 = Dish(
        "Vegemite Scoll",
        "VS5",
        mutableMapOf(
            5 to Pack(5, 899),
            3 to Pack(3, 699)
        )
    )
    val mb11 = Dish(
        "Blueberry Muffin",
        "MB11",
        mutableMapOf(
            8 to Pack(8, 2495),
            5 to Pack(5, 1695),
            2 to Pack(2, 995)
        )
    )
    val cf = Dish(
        "Croissant",
        "CF",
        mutableMapOf(
            9 to Pack(9, 1699),
            5 to Pack(5, 995),
            3 to Pack(3, 595)
        )
    )

    return mutableMapOf(
        "VS5" to vs5,
        "MB11" to mb11,
        "CF" to cf
    )
}

fun currencyPrint(amount: Int): String {
    val decimal = (amount % 100).toString().padStart(2, '0')
    return "\$${amount / 100}.${decimal}"
}